//!

#[aoc_generator(day14)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

/// Part 1
#[aoc(day14, part1)]
fn part1(input: &str) -> usize {
    let result: Vec<_> = input.lines().collect();

    21
}

/// Part 2
#[aoc(day14, part2)]
fn part2(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();
    11
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "498,4 -> 498,6 -> 496,6
    503,4 -> 502,4 -> 502,9 -> 494,9";

    #[test]
    fn part1_examples() {
        //assert_eq!(24000, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(45000, part2(&parse_input(EXAMPLE)));
    }
}
