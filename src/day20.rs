//!

use std::collections::VecDeque;

#[aoc_generator(day20)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

struct Node {
    pos: i32,
    value: i32,
}

struct Buffer {
    entries: i32,
    nodes: VecDeque<Node>,
}

impl Buffer {
    fn new(entries: i32) -> Buffer {
        let nodes: VecDeque<Node> = VecDeque::new();
        let valve = Buffer { entries, nodes };
        valve
    }
    fn add(&mut self, value: i32) {
        let node = Node {
            pos: self.entries,
            value,
        };
        self.nodes.push_back(node);
        self.entries += 1;
    }

    fn solution(&mut self) -> i32 {
        for index in 0..self.nodes.len() {
            if self.nodes[index].value == 0 {
                println!(
                    "{}",
                    self.nodes[((index + 1000) % self.nodes.len()) as usize].value
                );
                println!(
                    "{}",
                    self.nodes[((index + 2000) % self.nodes.len()) as usize].value
                );
                println!(
                    "{}",
                    self.nodes[((index + 3000) % self.nodes.len()) as usize].value
                );

                return self.nodes[((index + 1000) % self.nodes.len()) as usize].value
                    + self.nodes[((index + 2000) % self.nodes.len()) as usize].value
                    + self.nodes[((index + 3000) % self.nodes.len()) as usize].value;
            }
        }

        1
    }

    fn shuffle(&mut self) {}
    fn move_item(&mut self, pos: i32, counts: i32) {
        //println!(" ");
        //println!("move stuff {} {}", pos, counts);
        if counts > 0 {
            for steps in 0..counts {
                for index in 0..self.nodes.len() {
                    if self.nodes[index].pos == pos {
                        //if index == self.nodes.len()-1 {
                        if false {
                            //println!("flip");
                            let copy = self.nodes.pop_back().unwrap();
                            self.nodes.push_front(copy);

                            let index = 0;

                            //println!("move stuff {} index {}", pos, index);
                            let next_pos = self.nodes[(index + 1) % self.entries as usize].pos;
                            let next_value = self.nodes[(index + 1) % self.entries as usize].value;
                            self.nodes[(index + 1) % self.entries as usize].pos =
                                self.nodes[index].pos;
                            self.nodes[(index + 1) % self.entries as usize].value =
                                self.nodes[index].value;
                            self.nodes[index].pos = next_pos;
                            self.nodes[index].value = next_value;
                        } else {
                            //println!("move stuff {} index {}", pos, index);
                            let next_pos = self.nodes[(index + 1) % self.entries as usize].pos;
                            let next_value = self.nodes[(index + 1) % self.entries as usize].value;
                            self.nodes[(index + 1) % self.entries as usize].pos =
                                self.nodes[index].pos;
                            self.nodes[(index + 1) % self.entries as usize].value =
                                self.nodes[index].value;
                            self.nodes[index].pos = next_pos;
                            self.nodes[index].value = next_value;
                        }

                        break;
                    }
                }
            }
        } else {
            for steps in 0..counts * -1 {
                for index in 0..self.nodes.len() {
                    if self.nodes[index].pos == pos {
                        if index == 0 {
                            //println!("flip");
                            let copy = self.nodes.pop_front().unwrap();
                            self.nodes.push_back(copy);

                            let index = self.nodes.len() - 1;

                            //println!("move stuff {} index {}", pos, index);
                            let length = self.nodes.len();
                            let next_pos =
                                self.nodes[(index + length - 1) % self.entries as usize].pos;
                            let next_value =
                                self.nodes[(index + length - 1) % self.entries as usize].value;
                            self.nodes[(index + length - 1) % self.entries as usize].pos =
                                self.nodes[index].pos;
                            self.nodes[(index + length - 1) % self.entries as usize].value =
                                self.nodes[index].value;
                            self.nodes[index].pos = next_pos;
                            self.nodes[index].value = next_value;
                        } else {
                            //println!("move stuff {} index {}", pos, index);
                            let length = self.nodes.len();
                            let next_pos =
                                self.nodes[(index + length - 1) % self.entries as usize].pos;
                            let next_value =
                                self.nodes[(index + length - 1) % self.entries as usize].value;
                            self.nodes[(index + length - 1) % self.entries as usize].pos =
                                self.nodes[index].pos;
                            self.nodes[(index + length - 1) % self.entries as usize].value =
                                self.nodes[index].value;
                            self.nodes[index].pos = next_pos;
                            self.nodes[index].value = next_value;
                        }

                        break;
                    }
                }
            }
        }
    }
    fn show(&mut self) {
        for node in self.nodes.iter() {
            //println!("{} {} {} {}", node.pos, node.value, node.next, node.previous);
            print!("{} ", node.value);
        }
        println!(" ");
        println!(" ");
    }
    fn open(&mut self) {}
}

/// Part 1
#[aoc(day20, part1)]
fn part1(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();
    let mut buffer = Buffer::new(0);
    let mut values: Vec<i32> = Vec::new();
    for line in lines {
        match line.trim().parse::<i32>() {
            Ok(value) => {
                println!("val: {}", value);
                buffer.add(value);
                values.push(value);
            }
            _ => {
                println!("error");
            }
        }
    }

    for index in 0..values.len() {
        buffer.move_item(index as i32, values[index]);
    }
    buffer.show();

    buffer.solution()
}

/// Part 2
#[aoc(day20, part2)]
fn part2(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();
    11
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "1
    2
    -3
    3
    -2
    0
    4";

    #[test]
    fn part1_examples() {
        assert_eq!(3, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(45000, part2(&parse_input(EXAMPLE)));
    }
}
