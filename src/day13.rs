//!

#[aoc_generator(day13)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn process_line(line: &str) {
    println!("{:?}", line);
    for ch in line.chars() {
        if ch == '[' {
            println!("open");
        } else if ch == ']' {
            println!("close");
        } else if ch == ',' {
            println!("comma");
        } else {
            println!("nr");
        }
    }
}

/// Part 1
#[aoc(day13, part1)]
fn part1(input: &str) -> usize {
    let result: Vec<_> = input.lines().collect();

    //for line in 0..result.len() {
    for line in 0..3 {
        if line % 3 == 0 {
            println!("first line");
            process_line(result[line]);
        } else if line % 3 == 1 {
            println!("second line");
            process_line(result[line]);
        }
    }
    21
}

/// Part 2
#[aoc(day13, part2)]
fn part2(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();
    11
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";

    #[test]
    fn part1_examples() {
        //assert_eq!(24000, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(45000, part2(&parse_input(EXAMPLE)));
    }
}
