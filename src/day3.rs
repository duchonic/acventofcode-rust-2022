//!
#[aoc_generator(day3)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn process(input: &str) -> u32 {
    let result: Vec<_> = input.lines().collect();
    let mut sum: u32 = 0;
    for item in result {
        let mut first: Vec<u8> = Vec::new();
        let mut second: Vec<u8> = Vec::new();

        for b in item.bytes() {
            first.push(b);
            second.push(b);
        }
        let half = first.len() / 2 - 1;
        let mut found = false;
        println!("half {}", half);
        for (f_index, f) in first.iter().enumerate() {
            for (s_index, s) in second.iter().enumerate() {
                if f == s && s_index != f_index && f_index <= half && s_index > half {
                    let mut score = *f - 65 + 27;
                    if score > 52 {
                        score -= 58;
                    }
                    sum += score as u32;
                    found = true;
                    break;
                }
            }
            if found {
                break;
            }
        }
    }
    sum
}

/// Part 1
#[aoc(day3, part1)]
fn part1(input: &str) -> u32 {
    println!("PART 1");
    process(input)
}

/// Part 2
#[aoc(day3, part2)]
fn part2(input: &str) -> u32 {
    let result: Vec<_> = input.lines().collect();

    let mut first: Vec<u8> = Vec::new();
    let mut second: Vec<u8> = Vec::new();
    let mut third: Vec<u8> = Vec::new();
    let mut sum: u32 = 0;

    for item in result {
        if first.is_empty() {
            println!("first {}", item);
            for byte in item.bytes() {
                first.push(byte);
            }
        } else if second.is_empty() {
            println!("second {}", item);
            for byte in item.bytes() {
                second.push(byte);
            }
        } else if third.is_empty() {
            println!("third {}", item);
            for byte in item.bytes() {
                third.push(byte);
            }

            let mut found = false;
            for f in &first {
                for s in &second {
                    for t in &third {
                        if f == s && s == t {
                            println!("match {}", f);
                            let mut score = *f - 65 + 27;
                            if score > 52 {
                                score -= 58;
                            }
                            sum += score as u32;
                            found = true;
                            break;
                        }
                    }
                    if found {
                        break;
                    }
                }
                if found {
                    break;
                }
            }
            //process
            println!("process first {:?}", first);

            first.clear();
            second.clear();
            third.clear();
        }
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

    #[test]
    fn part1_examples() {
        assert_eq!(157, part1(&parse_input(EXAMPLE)));
        assert_eq!(1, part1(&parse_input("baca")));
        assert_eq!(2, part1(&parse_input("abcb")));
        assert_eq!(26, part1(&parse_input("azcz")));
        assert_eq!(27, part1(&parse_input("AbcA")));
        assert_eq!(52, part1(&parse_input("obZZck")));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(70, part2(&parse_input(EXAMPLE)));
    }
}
