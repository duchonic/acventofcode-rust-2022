//!
use grid::Grid;

#[aoc_generator(day12)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

struct Size {
    width: usize,
    height: usize,
}

#[derive(Clone, Debug)]
struct Point {
    x: usize,
    y: usize,
}

struct Map {
    pub surface: Grid<char>,
    size: Size,
    start: Point,
    end: Point,
}

impl Map {
    fn new(size: Size) -> Map {
        let map = Map {
            surface: Grid::new(size.height, size.width),
            size,
            start: Point { x: 0, y: 0 },
            end: Point { x: 0, y: 0 },
        };
        map
    }
    fn set(&mut self, pos: Point, value: char) {
        if value == 'S' {
            self.start = pos.clone();
        } else if value == 'E' {
            self.end = pos.clone();
        }
        self.surface[pos.y][pos.x] = value;
    }
    fn show(&self) {
        println!("");
        println!("shwo from stuct stuff:");
        for y in 0..self.size.height {
            for x in 0..self.size.width {
                print!("{}", self.surface[y][x]);
            }
            println!("");
        }
        println!("start {:?} end {:?}", self.start, self.end);
    }

    fn check_neighbours(self, _pos: Point) -> Vec<Point> {
        // check left, right, down and up
        let check: Vec<Point> = Vec::new();
        for _neighbour in [(-1, 0), (1, 0), (0, 1), (0, -1)] {}
        check
    }
}

/// Part 1
#[aoc(day12, part1)]
fn part1(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();

    let mut m = Map::new(Size {
        height: result.len(),
        width: result[0].len(),
    });

    for (y, line) in result.iter().enumerate() {
        for (x, ch) in line.to_string().chars().enumerate() {
            m.set(Point { x, y }, ch);
        }
    }
    m.show();
    m.check_neighbours(Point { x: 1, y: 1 });
    12
}

/// Part 2
#[aoc(day12, part2)]
fn part2(_input: &str) -> i32 {
    //let result: Vec<_> = input.lines().collect();
    31
}

#[cfg(test)]
mod tests {
    use super::*;

    /*
    const EXAMPLE: &str = "Sabqponm
    abcryxxl
    accszExk
    acctuvwj
    abdefghi";
    */

    #[test]
    fn part1_examples() {
        //assert_eq!(31, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(12, part2(&parse_input(EXAMPLE)));
    }

    #[test]
    fn map1_examples() {
        let mut m = Map::new(Size {
            height: 2,
            width: 10,
        });
        m.set(Point { x: 8, y: 1 }, 'a');
        m.set(Point { y: 0, x: 4 }, 'b');
        //assert_eq!('a', m.get(Point { y: 1, x: 8 }));
        //assert_eq!('b', m.get(Point { y: 0, x: 4 }));
        //assert_eq!('b', m.get(Point { x: 4, y: 0 }));
    }

    #[test]
    fn map2_examples() {
        let mut m = Map::new(Size {
            width: 10,
            height: 5,
        });
        m.set(Point { x: 8, y: 1 }, 'a');
        m.set(Point { y: 0, x: 4 }, 'b');
        assert_eq!('a', m.surface[1][8]);
        //assert_eq!('a', m.get(Point { y: 1, x: 8 }));
        //assert_eq!('b', m.get(Point { y: 0, x: 4 }));
        //assert_eq!('b', m.get(Point { x: 4, y: 0 }));

        m.check_neighbours(Point { x: 2, y: 2 });
    }

    #[test]
    fn chars_compare() {
        assert!('0' < 'A');
        assert!('Y' < 'a');
        assert!('a' < 'b');
        assert!('x' < '{');
    }
}
