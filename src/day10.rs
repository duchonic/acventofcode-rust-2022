//!
#[aoc_generator(day10)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn check(pc: i32) -> bool {
    pc == 20 || pc == 60 || pc == 100 || pc == 140 || pc == 180 || pc == 220
}

fn line_check(pc: i32) -> bool {
    pc == 40 || pc == 80 || pc == 120 || pc == 160 || pc == 200 || pc == 240
}
/// Part 1
#[aoc(day10, part1)]
fn part1(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();

    let mut register = 1;
    let mut pc = 0;
    let mut tests: Vec<i32> = Vec::new();

    for line in lines {
        let cmd: Vec<&str> = line.trim().split(' ').collect();
        if cmd.len() == 2 {
            let command: String = cmd[0].parse::<String>().unwrap();
            let value: i32 = cmd[1].parse::<i32>().unwrap();
            println!("{} {}", command, value);

            pc += 1;
            if check(pc) {
                tests.push(register * pc);
            }

            pc += 1;
            if check(pc) {
                tests.push(register * pc);
            }
            register += value;
            println!("register:{}", value);
        } else {
            println!("nop");
            pc += 1;
            if check(pc) {
                tests.push(register * pc);
            }
        }
    }
    println!("tests {:?}", tests);
    let mut sum = 0;
    for nr in tests {
        sum += nr;
    }
    sum
}

/// Part 2
#[aoc(day10, part2)]
fn part2(input: &str) -> String {
    let lines: Vec<_> = input.lines().collect();

    let mut register = 1;
    let mut pixel = 0;
    let mut pixel_line: String = String::from("");

    for line in lines {
        let cmd: Vec<&str> = line.trim().split(' ').collect();
        if cmd.len() == 2 {
            let value: i32 = cmd[1].parse::<i32>().unwrap();

            if pixel % 40 == register || pixel % 40 == register - 1 || pixel % 40 == register + 1 {
                pixel_line.push('★');
            } else {
                pixel_line.push(' ');
            }
            pixel += 1;
            if line_check(pixel) {
                println!("{}", pixel_line);
                pixel_line.clear();
            }
            if pixel % 40 == register || pixel % 40 == register - 1 || pixel % 40 == register + 1 {
                pixel_line.push('★');
            } else {
                pixel_line.push(' ');
            }
            pixel += 1;
            if line_check(pixel) {
                println!("{}", pixel_line);
                pixel_line.clear();
            }

            register += value;
        } else {
            if pixel % 40 == register || pixel % 40 == register - 1 || pixel % 40 == register + 1 {
                pixel_line.push('★');
            } else {
                pixel_line.push(' ');
            }
            pixel += 1;
            if line_check(pixel) {
                println!("{}", pixel_line);
                pixel_line.clear();
            }
        }
    }
    pixel_line
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "addx 15
    addx -11
    addx 6
    addx -3
    addx 5
    addx -1
    addx -8
    addx 13
    addx 4
    noop
    addx -1
    addx 5
    addx -1
    addx 5
    addx -1
    addx 5
    addx -1
    addx 5
    addx -1
    addx -35
    addx 1
    addx 24
    addx -19
    addx 1
    addx 16
    addx -11
    noop
    noop
    addx 21
    addx -15
    noop
    noop
    addx -3
    addx 9
    addx 1
    addx -3
    addx 8
    addx 1
    addx 5
    noop
    noop
    noop
    noop
    noop
    addx -36
    noop
    addx 1
    addx 7
    noop
    noop
    noop
    addx 2
    addx 6
    noop
    noop
    noop
    noop
    noop
    addx 1
    noop
    noop
    addx 7
    addx 1
    noop
    addx -13
    addx 13
    addx 7
    noop
    addx 1
    addx -33
    noop
    noop
    noop
    addx 2
    noop
    noop
    noop
    addx 8
    noop
    addx -1
    addx 2
    addx 1
    noop
    addx 17
    addx -9
    addx 1
    addx 1
    addx -3
    addx 11
    noop
    noop
    addx 1
    noop
    addx 1
    noop
    noop
    addx -13
    addx -19
    addx 1
    addx 3
    addx 26
    addx -30
    addx 12
    addx -1
    addx 3
    addx 1
    noop
    noop
    noop
    addx -9
    addx 18
    addx 1
    addx 2
    noop
    noop
    addx 9
    noop
    noop
    noop
    addx -1
    addx 2
    addx -37
    addx 1
    addx 3
    noop
    addx 15
    addx -21
    addx 22
    addx -6
    addx 1
    noop
    addx 2
    addx 1
    noop
    addx -10
    noop
    noop
    addx 20
    addx 1
    addx 2
    addx 2
    addx -6
    addx -11
    noop
    noop
    noop";

    #[test]
    fn part1_examples() {
        assert_eq!(13140, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!("##..##..##..##..##..#".to_string(), part2(&parse_input(EXAMPLE)));
    }
}
