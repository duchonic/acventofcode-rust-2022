//!

use std::collections::VecDeque;
#[aoc_generator(day11)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

/// Part 1
#[aoc(day11, part1)]
fn part1(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();
    let mut monkeys: Vec<VecDeque<i32>> = Vec::new();

    let mut monkey: usize = 0;

    let mut operator: Vec<&str> = Vec::new();
    let mut operator_val: Vec<i32> = Vec::new();

    let mut divisor: Vec<i32> = Vec::new();
    let mut condition: Vec<Vec<usize>> = Vec::new();

    let mut counter: Vec<i32> = Vec::new();

    for line in lines {
        let cmd: Vec<&str> = line.trim().split(' ').collect();

        if cmd[0] == "Monkey".to_string() {
            monkey = cmd[1].parse::<usize>().unwrap();
            monkeys.push(VecDeque::new());
            condition.push(vec![]);
            counter.push(0);

            println!("{:?}", cmd);
        } else if cmd[0] == "Starting".to_string() {
            println!("starting items");
            println!("{:?}", cmd);
            for item in 2..cmd.len() {
                let result = str::replace(cmd[item], ",", "");

                let value = result.parse::<i32>().unwrap();
                monkeys[monkey].push_back(value);
            }
        } else if cmd[0] == "Operation:".to_string() {
            println!("operation");

            match cmd[5].parse::<i32>() {
                Ok(nr) => {
                    operator_val.push(nr);
                    operator.push(cmd[4]);
                }
                _ => {
                    operator_val.push(-1);
                    operator.push("square");
                }
            }

            println!("{:?}", cmd);
        } else if cmd[0] == "Test:".to_string() {
            println!("test");
            println!("{:?}", cmd);
            divisor.push(cmd[3].parse::<i32>().unwrap());
        } else if cmd[0] == "If".to_string() {
            println!("if");
            println!("{:?}", cmd);

            if cmd[1] == "true:" {
                condition[monkey].push(cmd[5].parse::<usize>().unwrap());
            } else if cmd[1] == "false:" {
                condition[monkey].push(cmd[5].parse::<usize>().unwrap());
            }
        }
    }
    println!("monkeys : {:?}", monkeys);

    println!("operator : {:?}", operator);
    println!("operator_val : {:?}", operator_val);

    println!("divisor : {:?}", divisor);
    println!("condition : {:?}", condition);

    for _ in 0..20 {
        for monkey in 0..8 {
            while monkeys[monkey].len() > 0 {
                counter[monkey] += 1;
                let mut value = monkeys[monkey].pop_front().unwrap();
                if operator[monkey] == "+" {
                    value += operator_val[monkey];
                } else if operator[monkey] == "-" {
                    value -= operator_val[monkey];
                } else if operator[monkey] == "*" {
                    value *= operator_val[monkey];
                } else if operator[monkey] == "/" {
                    value /= operator_val[monkey];
                } else {
                    value *= value;
                }
                value /= 3;

                if value % divisor[monkey] == 0 {
                    monkeys[condition[monkey][0]].push_back(value);
                } else {
                    monkeys[condition[monkey][1]].push_back(value);
                }
            }
        }
    }
    counter.sort();

    println!("monkeys : {:?}", monkeys);
    println!("counter : {:?}", counter);
    counter[6] * counter[7]
}

/// Part 2
#[aoc(day11, part2)]
fn part2(input: &str) -> i128 {
    let lines: Vec<_> = input.lines().collect();
    let mut monkeys: Vec<VecDeque<i128>> = Vec::new();

    let mut monkey: usize = 0;

    let mut operator: Vec<&str> = Vec::new();
    let mut operator_val: Vec<i128> = Vec::new();

    let mut divisor: Vec<i128> = Vec::new();
    let mut condition: Vec<Vec<usize>> = Vec::new();

    let mut counter: Vec<i128> = Vec::new();

    for line in lines {
        let cmd: Vec<&str> = line.trim().split(' ').collect();

        if cmd[0] == "Monkey".to_string() {
            monkey = cmd[1].parse::<usize>().unwrap();
            monkeys.push(VecDeque::new());
            condition.push(vec![]);
            counter.push(0);

            println!("{:?}", cmd);
        } else if cmd[0] == "Starting".to_string() {
            println!("starting items");
            println!("{:?}", cmd);
            for item in 2..cmd.len() {
                let result = str::replace(cmd[item], ",", "");

                let value = result.parse::<i128>().unwrap();
                monkeys[monkey].push_back(value);
            }
        } else if cmd[0] == "Operation:".to_string() {
            println!("operation");

            match cmd[5].parse::<i128>() {
                Ok(nr) => {
                    operator_val.push(nr);
                    operator.push(cmd[4]);
                }
                _ => {
                    operator_val.push(-1);
                    operator.push("square");
                }
            }

            println!("{:?}", cmd);
        } else if cmd[0] == "Test:".to_string() {
            println!("test");
            println!("{:?}", cmd);
            divisor.push(cmd[3].parse::<i128>().unwrap());
        } else if cmd[0] == "If".to_string() {
            println!("if");
            println!("{:?}", cmd);

            if cmd[1] == "true:" {
                condition[monkey].push(cmd[5].parse::<usize>().unwrap());
            } else if cmd[1] == "false:" {
                condition[monkey].push(cmd[5].parse::<usize>().unwrap());
            }
        }
    }
    println!("monkeys : {:?}", monkeys);

    println!("operator : {:?}", operator);
    println!("operator_val : {:?}", operator_val);

    println!("divisor : {:?}", divisor);
    println!("condition : {:?}", condition);

    let mut lcm = 1;
    for div in &divisor {
        lcm *= div;
    }
    divisor.push(lcm.clone());

    for round in 1..=10000 {
        //for round in 1..=20 {
        for monkey in 0..8 {
            while monkeys[monkey].len() > 0 {
                counter[monkey] += 1;
                let mut value = monkeys[monkey].pop_front().unwrap();
                if operator[monkey] == "+" {
                    value += operator_val[monkey];
                } else if operator[monkey] == "-" {
                    value -= operator_val[monkey];
                } else if operator[monkey] == "*" {
                    value *= operator_val[monkey];
                } else if operator[monkey] == "/" {
                    value /= operator_val[monkey];
                } else {
                    value *= value;
                }

                value %= divisor[8];

                if value % divisor[monkey] == 0 {
                    let to_monkey = condition[monkey][0];
                    monkeys[to_monkey].push_back(value); //% divisor[to_monkey] );
                } else {
                    let to_monkey = condition[monkey][1];
                    monkeys[to_monkey].push_back(value); //% divisor[to_monkey] );
                }
            }
        }
        //println!("counter : {:?}", counter);
        if round == 1 || round == 20 || round == 1000 || round == 2000 || round == 3000 {
            println!("{}", round);
            println!("counter : {:?}", counter);
            println!("monkeys : {:?}", monkeys);
        }
    }
    counter.sort();

    println!("monkeys : {:?}", monkeys);
    counter[6] * counter[7]
}

#[cfg(test)]
mod tests {
    //use super::*;

    /*
        const EXAMPLE: &str = "Monkey 0
      Starting items: 79, 98
      Operation: new = old * 19
      Test: divisible by 23
        If true: throw to monkey 2
        If false: throw to monkey 3

    Monkey 1
      Starting items: 54, 65, 75, 74
      Operation: new = old + 6
      Test: divisible by 19
        If true: throw to monkey 2
        If false: throw to monkey 0

    Monkey 2
      Starting items: 79, 60, 97
      Operation: new = old * old
      Test: divisible by 13
        If true: throw to monkey 1
        If false: throw to monkey 3

    Monkey 3
      Starting items: 74
      Operation: new = old + 3
      Test: divisible by 17
        If true: throw to monkey 0
        If false: throw to monkey 1";
        */

    #[test]
    fn part1_examples() {
        //assert_eq!(10605, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(2713310158, part2(&parse_input(EXAMPLE)));
    }
}
