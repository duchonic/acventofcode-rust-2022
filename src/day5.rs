use std::collections::VecDeque;

#[aoc_generator(day5)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

struct Stack {
    stack: Vec<VecDeque<char>>,
}

impl Stack {
    fn new(size: usize) -> Stack {
        let mut new_stack = Stack { stack: Vec::new() };
        for _ in 0..=size {
            new_stack.stack.push(VecDeque::new());
        }
        new_stack
    }
    fn add(&mut self, index: usize, value: char) {
        assert!(index < self.stack.len());
        if index < self.stack.len() {
            self.stack[index].push_front(value);
        } else {
            println!("warning, register unknown");
        }
    }
    //fn print(&self) {
    //    println!("Stack {:?}", self.stack);
    //}
    fn move_item(&mut self, items: usize, from: usize, to: usize) {
        for _ in 0..items {
            if !self.stack[from].is_empty() {
                let item = self.stack[from].pop_back().unwrap();
                self.stack[to].push_back(item);
            } else {
                println!("warning, nothing to move {} {} {}", items, from, to);
            }
        }
    }
    fn move_item_reverse(&mut self, items: usize, from: usize, to: usize) {
        let mut copy_vec: VecDeque<char> = VecDeque::new();
        for _ in 0..items {
            if !self.stack[from].is_empty() {
                let item = self.stack[from].pop_back().unwrap();
                copy_vec.push_front(item);
            } else {
                println!("warning, nothing to move {} {} {}", items, from, to);
            }
        }
        for copy_item in copy_vec {
            self.stack[to].push_back(copy_item);
        }
    }
    fn get_top(&self) -> String {
        let mut result: String = String::from("");
        for line in self.stack.iter() {
            if !line.is_empty() {
                result.push(line[line.len() - 1]);
            }
        }
        result
    }
}

/// Part 1
#[aoc(day5, part1)]
fn part1(input: &str) -> String {
    let mut stack: Stack = Stack::new(10);
    let lines: Vec<_> = input.lines().collect();
    for line in lines {
        let numbers = line.trim().split(' ');
        if line.contains('[') {
            for (index, number) in numbers.enumerate() {
                let letter = number.chars().nth(1).unwrap();
                if letter != ']' {
                    stack.add(index, number.chars().nth(1).unwrap());
                }
            }
        } else if line.contains("move") {
            let mut command: Vec<usize> = Vec::new();
            for number in numbers {
                match number.parse::<usize>() {
                    Ok(nr) => command.push(nr),
                    _ => print!(""),
                }
            }
            stack.move_item(command[0], command[1] - 1, command[2] - 1);
        }
    }
    stack.get_top()
}

/// Part 2
#[aoc(day5, part2)]
fn part2(input: &str) -> String {
    let mut stack: Stack = Stack::new(10);
    let lines: Vec<_> = input.lines().collect();
    for line in lines {
        let numbers = line.trim().split(' ');
        if line.contains('[') {
            for (index, number) in numbers.enumerate() {
                let letter = number.chars().nth(1).unwrap();
                if letter != ']' {
                    stack.add(index, number.chars().nth(1).unwrap());
                }
            }
        } else if line.contains("move") {
            let mut command: Vec<usize> = Vec::new();
            for number in numbers {
                match number.parse::<usize>() {
                    Ok(nr) => command.push(nr),
                    _ => print!(""),
                }
            }
            stack.move_item_reverse(command[0], command[1] - 1, command[2] - 1);
        }
    }
    stack.get_top()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = " [] [D] [] 
    [N] [C]    
    [Z] [M] [P]
     1   2   3 
    
    move 1 from 2 to 1
    move 3 from 1 to 3
    move 2 from 2 to 1
    move 1 from 1 to 2
    ";

    #[test]
    fn part1_examples() {
        assert_eq!("CMZ", part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!("MCD", part2(&parse_input(EXAMPLE)));
    }
}
