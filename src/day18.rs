//!

#[aoc_generator(day18)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

#[derive(Debug, PartialEq)]
struct Point {
    x: i32,
    y: i32,
    z: i32,
}
/// Part 1
#[aoc(day18, part1)]
fn part1(input: &str) -> usize {
    let lines: Vec<_> = input.lines().collect();
    let mut map: Vec<Point> = Vec::new();
    for line in lines {
        let cmd: Vec<&str> = line.trim().split(',').collect();
        if cmd.len() == 3 {
            let mut point: Point = Point { x: 0, y: 0, z: 0 };

            match cmd[0].parse::<i32>() {
                Ok(x) => point.x = x,
                _ => println!("error"),
            }
            match cmd[1].parse::<i32>() {
                Ok(y) => point.y = y,
                _ => println!("error"),
            }
            match cmd[2].parse::<i32>() {
                Ok(z) => point.z = z,
                _ => println!("error"),
            }
            map.push(point);
        }
    }
    println!("p: {:?}", map);

    let mut total_sides = map.len() * 6;

    for point in map.iter() {
        for check in map.iter() {
            if check != point {
                let distance = (check.x - point.x).abs()
                    + (check.y - point.y).abs()
                    + (check.z - point.z).abs();
                if distance == 1 {
                    println!(
                        "check distance {:?} -> {:?} is {:?}",
                        point, check, distance
                    );
                    total_sides -= 1;
                }
            }
        }
    }

    total_sides
}

/// Part 2
#[aoc(day18, part2)]
fn part2(input: &str) -> usize {
    let lines: Vec<_> = input.lines().collect();
    let mut map: Vec<Point> = Vec::new();
    for line in lines {
        let cmd: Vec<&str> = line.trim().split(',').collect();
        if cmd.len() == 3 {
            let mut point: Point = Point { x: 0, y: 0, z: 0 };
            match cmd[0].parse::<i32>() {
                Ok(x) => point.x = x,
                _ => println!("error"),
            }
            match cmd[1].parse::<i32>() {
                Ok(y) => point.y = y,
                _ => println!("error"),
            }
            match cmd[2].parse::<i32>() {
                Ok(z) => point.z = z,
                _ => println!("error"),
            }
            map.push(point);
        }
    }
    println!("p: {:?}", map);

    let mut total_sides = map.len() * 6;

    for point in map.iter() {
        for check in map.iter() {
            if check != point {
                let distance = (check.x - point.x).abs()
                    + (check.y - point.y).abs()
                    + (check.z - point.z).abs();
                if distance == 1 {
                    println!(
                        "check distance {:?} -> {:?} is {:?}",
                        point, check, distance
                    );
                    total_sides -= 1;
                }
            }
        }
    }

    total_sides
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "
    1,2,2
    1,2,5
    2,1,2
    2,1,5
    2,2,1
    2,2,2
    2,2,3
    2,2,5, this is a missing part, it has 6 sides touching another cube
    2,2,4
    2,2,6
    2,3,2
    2,3,5
    3,2,2
    3,2,5
    ";

    #[test]
    fn part1_examples() {
        assert_eq!(64, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(58, part2(&parse_input(EXAMPLE)));
    }
}
