use std::collections::VecDeque;
#[aoc_generator(day6)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn check_pairs(check: Vec<char>) -> bool {
    // TODO there must be a better way to check if x values are all different
    for index in 0..=check.len() - 2 {
        if check[index] == check[index + 1] {
            return true;
        }
    }
    false
}

fn find_package_start(input: &str, window: usize) -> usize {
    let mut check: VecDeque<char> = VecDeque::new();
    for (startpos, ch) in input.chars().enumerate() {
        if check.len() == window {
            let mut vec_sort: Vec<char> = Vec::from(check.clone());
            vec_sort.sort();
            if !check_pairs(vec_sort) {
                return startpos;
            }
            let _ = check.pop_back();
        }
        check.push_front(ch);
    }
    0
}

/// Part 1
#[aoc(day6, part1)]
fn part1(input: &str) -> usize {
    find_package_start(input, 4)
}

/// Part 2
#[aoc(day6, part2)]
fn part2(input: &str) -> usize {
    find_package_start(input, 14)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
    const EXAMPLE2: &str = "nppdvjthqldpwncqszvftbrmjlhg";

    #[test]
    fn part1_examples() {
        assert_eq!(7, part1(&parse_input(EXAMPLE)));
        assert_eq!(6, part1(&parse_input(EXAMPLE2)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(19, part2(&parse_input(EXAMPLE)));
        assert_eq!(23, part2(&parse_input(EXAMPLE2)));
    }
}
