//!
#[aoc_generator(day2)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn process(input: &str) -> u32 {
    let result: Vec<_> = input.lines().collect();
    let mut score_item = 0;
    let mut score_win = 0;
    for item in result {
        let play = item.split(' ');
        let mut enemy = -1;
        for t in play {
            if t == "X" {
                score_item += 1;
                if enemy == 1 {
                    score_win += 3;
                } else if enemy == 3 {
                    score_win += 6;
                }
            } else if t == "Y" {
                score_item += 2;
                if enemy == 2 {
                    score_win += 3;
                } else if enemy == 1 {
                    score_win += 6;
                }
            } else if t == "Z" {
                score_item += 3;
                if enemy == 3 {
                    score_win += 3;
                }
                if enemy == 2 {
                    score_win += 6;
                }
            } else if t == "A" {
                enemy = 1;
            } else if t == "B" {
                enemy = 2;
            } else if t == "C" {
                enemy = 3;
            }
        }
    }
    score_item + score_win
}

/// Part 1
#[aoc(day2, part1)]
fn part1(input: &str) -> u32 {
    process(input)
}

/// Part 2
#[aoc(day2, part2)]
fn part2(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();
    let mut score_item = 0;
    let mut score_win = 0;
    for item in result {
        let play = item.split(' ');
        let mut enemy: i32 = -1;
        for t in play {
            if t == "X" {
                //loose
                if enemy == 1 {
                    score_item += 3;
                } else if enemy == 2 {
                    score_item += 1;
                } else if enemy == 3 {
                    score_item += 2;
                }
            } else if t == "Y" {
                //draw
                score_win += 3;
                score_item += enemy;
            } else if t == "Z" {
                //win
                score_win += 6;
                if enemy == 1 {
                    score_item += 2;
                }
                if enemy == 2 {
                    score_item += 3;
                }
                if enemy == 3 {
                    score_item += 1;
                }
            } else if t == "A" {
                enemy = 1;
            } else if t == "B" {
                enemy = 2;
            } else if t == "C" {
                enemy = 3;
            }
        }
    }
    score_item + score_win
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "A Y
B X
C Z";

    #[test]
    fn part1_examples() {
        assert_eq!(15, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(12, part2(&parse_input(EXAMPLE)));
    }
}
