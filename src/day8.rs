//!

use grid::Grid;
#[aoc_generator(day8)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn score(map: Grid<i32>, pos: (usize, usize)) -> i32 {
    let mut view = 1;
    let mut total = 1;

    let tree = map[pos.1][pos.0];

    //to north
    for y in (1..pos.1).rev() {
        let check = map[y][pos.0];
        if check >= tree {
            break;
        } else {
            view += 1;
        }
    }
    total *= view;
    view = 1;

    //to south
    for y in pos.1 + 1..map.size().1 - 1 {
        let check = map[y][pos.0];
        if check >= tree {
            break;
        } else {
            view += 1;
        }
    }
    total *= view;
    view = 1;
    //go left
    for x in (1..pos.0).rev() {
        if map[pos.1][x] >= tree {
            break;
        } else {
            view += 1;
        }
    }
    total *= view;
    view = 1;
    //go right
    for x in pos.0 + 1..map.size().1 - 1 {
        if map[pos.1][x] >= tree {
            break;
        } else {
            view += 1;
        }
    }
    total *= view;

    total
}

fn see_the_edge(map: Grid<i32>, pos: (usize, usize)) -> bool {
    //go north (from top to middle alays up)
    let mut see_edge = true;
    for y in 0..pos.1 {
        if map[y][pos.0] >= map[pos.1][pos.0] {
            see_edge = false;
        }
    }
    if see_edge {
        return true;
    }

    see_edge = true;
    //go south (from middle to bottom always down)
    for y in pos.1 + 1..map.size().1 {
        if map[y][pos.0] >= map[pos.1][pos.0] {
            see_edge = false;
        }
    }
    if see_edge {
        return true;
    }

    see_edge = true;
    //go left from left edge to middle (always up)
    for x in 0..pos.0 {
        if map[pos.1][x] >= map[pos.1][pos.0] {
            see_edge = false;
        }
    }
    if see_edge {
        return true;
    }

    see_edge = true;
    //go right (from middle to right edge always down)
    for x in pos.0 + 1..map.size().0 {
        if map[pos.1][x] >= map[pos.1][pos.0] {
            see_edge = false;
        }
    }
    if see_edge {
        return true;
    }
    false
}

/// Part 1
#[aoc(day8, part1)]
fn part1(input: &str) -> usize {
    let lines: Vec<_> = input.lines().collect();

    let mut l: Vec<i32> = Vec::new();

    let mut nr_of_lines = 0;
    for line in lines {
        for nr in line.chars() {
            l.push(nr.to_string().parse::<i32>().unwrap());
        }
        nr_of_lines += 1;
    }
    let grid = Grid::from_vec(l, nr_of_lines);
    let mut checks: Grid<i32> = Grid::new(nr_of_lines, nr_of_lines);

    let mut trees = 0;
    for y in 1..nr_of_lines - 1 {
        for x in 1..nr_of_lines - 1 {
            if see_the_edge(grid.clone(), (x, y)) {
                checks[y][x] = 1;
                trees += 1;
            }
        }
    }

    trees + (4 * (nr_of_lines - 1))
}

/// Part 2
#[aoc(day8, part2)]
fn part2(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();
    let mut l: Vec<i32> = Vec::new();

    let mut nr_of_lines = 0;
    for line in lines {
        for nr in line.chars() {
            l.push(nr.to_string().parse::<i32>().unwrap());
        }
        nr_of_lines += 1;
    }

    let grid = Grid::from_vec(l, nr_of_lines);
    let mut checks: Grid<i32> = Grid::new(nr_of_lines, nr_of_lines);

    for y in 1..nr_of_lines - 1 {
        for x in 1..nr_of_lines - 1 {
            checks[y][x] = score(grid.clone(), (x, y));
        }
    }

    let mut part2 = 0;
    for y in 0..nr_of_lines {
        for x in 0..nr_of_lines {
            if checks[y][x] > part2 {
                part2 = checks[y][x];
            }
        }
    }
    part2
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "30373
25512
65332
33549
35390";

    #[test]
    fn part1_examples() {
        assert_eq!(21, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(8, part2(&parse_input(EXAMPLE)));
    }
}
