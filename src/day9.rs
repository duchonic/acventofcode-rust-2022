//!
use std::collections::HashMap;

#[aoc_generator(day9)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

/// Part 1
#[aoc(day9, part1)]
fn part1(input: &str) -> usize {
    let lines: Vec<_> = input.lines().collect();
    let mut my_map: HashMap<(i32, i32), i32> = HashMap::new();
    let mut head: (i32, i32) = (0, 0);
    let mut tail: (i32, i32) = (0, 0);

    for (nr, line) in lines.iter().enumerate() {
        let cmd: Vec<&str> = line.trim().split(' ').collect();
        println!("{} {:?}", nr, cmd);
        let direction: char = cmd[0].parse::<char>().unwrap();
        let distance: i32 = cmd[1].parse::<i32>().unwrap();

        for _ in 0..distance {
            match direction {
                'U' => head.1 += 1,
                'D' => head.1 -= 1,
                'L' => head.0 -= 1,
                'R' => head.0 += 1,
                _ => println!("unknown"),
            }

            let d_vec: (i32, i32) = ((head.0 - tail.0), (head.1 - tail.1));

            if d_vec.0 == 0 {
                //only move y
                tail.1 += d_vec.1 / 2;
            } else if d_vec.1 == 0 {
                //only move x
                tail.0 += d_vec.0 / 2;
            } else {
                //move diagonal
                if d_vec.0.abs() == 1 && d_vec.1.abs() == 2 {
                    tail.0 += d_vec.0;
                    tail.1 += d_vec.1 / 2;
                } else if d_vec.1.abs() == 1 && d_vec.0.abs() == 2 {
                    tail.0 += d_vec.0 / 2;
                    tail.1 += d_vec.1;
                }
            }
            my_map.insert(tail, 1);
            println!(
                "head pos : {:?} tail pos : {:?} 2d_vec {:?}",
                head, tail, d_vec
            );
        }
    }
    my_map.len()
}

/// Part 2
#[aoc(day9, part2)]
fn part2(input: &str) -> usize {
    let lines: Vec<_> = input.lines().collect();
    let mut my_map: HashMap<(i32, i32), i32> = HashMap::new();

    let mut rope: [(i32, i32); 10] = [(0, 0); 10];

    for (nr, line) in lines.iter().enumerate() {
        let cmd: Vec<&str> = line.trim().split(' ').collect();
        println!("{} {:?}", nr, cmd);
        let direction: char = cmd[0].parse::<char>().unwrap();
        let distance: i32 = cmd[1].parse::<i32>().unwrap();

        for _ in 0..distance {
            match direction {
                'U' => rope[0].1 += 1,
                'D' => rope[0].1 -= 1,
                'L' => rope[0].0 -= 1,
                'R' => rope[0].0 += 1,
                _ => println!("unknown"),
            }

            for tail in 1..rope.len() {
                //println!("tail: {}", tail);
                let d_vec: (i32, i32) = (
                    (rope[tail - 1].0 - rope[tail].0),
                    (rope[tail - 1].1 - rope[tail].1),
                );

                if d_vec.0 == 0 {
                    //only move y
                    rope[tail].1 += d_vec.1 / 2;
                } else if d_vec.1 == 0 {
                    //only move x
                    rope[tail].0 += d_vec.0 / 2;
                } else {
                    //move diagonal
                    if d_vec.0.abs() == 1 && d_vec.1.abs() == 2 {
                        rope[tail].0 += d_vec.0;
                        rope[tail].1 += d_vec.1 / 2;
                    } else if d_vec.1.abs() == 1 && d_vec.0.abs() == 2 {
                        rope[tail].0 += d_vec.0 / 2;
                        rope[tail].1 += d_vec.1;
                    } else {
                        rope[tail].0 += d_vec.0 / 2;
                        rope[tail].1 += d_vec.1 / 2;
                    }
                }
            }
            my_map.insert(rope[9], 1);
        }
    }
    my_map.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";

    const EXAMPLE_LARGE: &str = "R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";

    #[test]
    fn part1_examples() {
        assert_eq!(13, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(1, part2(&parse_input(EXAMPLE)));
        assert_eq!(36, part2(&parse_input(EXAMPLE_LARGE)));
    }
}
