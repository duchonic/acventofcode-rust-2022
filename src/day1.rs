//!

#[aoc_generator(day1)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

/// Part 1
#[aoc(day1, part1)]
fn part1(input: &str) -> usize {
    let result: Vec<_> = input.lines().collect();
    let mut max = 0;
    let mut total = 0;
    for item in result {
        if item.is_empty() {
            if total > max {
                max = total;
            }
            total = 0;
        } else {
            total += item.parse::<i32>().unwrap();
        }
    }
    max as usize
}

/// Part 2
#[aoc(day1, part2)]
fn part2(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();
    let mut max: Vec<i32> = Vec::new();
    let mut total = 0;
    for item in result {
        if item.is_empty() {
            max.push(total);
            total = 0;
        } else {
            total += item.parse::<i32>().unwrap();
        }
    }
    max.push(total);

    max.sort();

    max[max.len() - 1] + max[max.len() - 2] + max[max.len() - 3]
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
";

    #[test]
    fn part1_examples() {
        assert_eq!(24000, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(45000, part2(&parse_input(EXAMPLE)));
    }
}
