use std::collections::HashMap;

#[aoc_generator(day7)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

fn get_path(input: &Vec<String>) -> String {
    let mut path: String = String::from('/');

    for folder in input {
        path += folder;
        path += &'/'.to_string();
    }
    path
}

fn calc_result(input: &str, part: u8) -> i64 {
    let lines: Vec<_> = input.lines().collect();
    let mut total_size: i64 = 0;

    let mut current_path: Vec<String> = Vec::new();
    let mut my_map: HashMap<String, i64> = HashMap::new();

    for line in lines {
        if line.contains("dir") {
            // add a directory to current directory
            total_size = 0;
        } else if line.contains("$ cd /") {
            current_path.clear();
            println!("process change directory");
        } else if line.contains("$ cd") {
            let cmds = line.trim().split(' ');
            for (index, cmd) in cmds.enumerate() {
                if index == 2 {
                    if cmd.contains("..") {
                        current_path.pop();
                    } else {
                        current_path.push(cmd.to_string());
                    }
                }
            }
        } else {
            let file_size = line.trim().split(' ');
            for item in file_size {
                match item.parse::<i64>() {
                    Ok(nr) => {
                        if my_map.contains_key("/") {
                            let val = my_map.get("/").unwrap();
                            my_map.insert("/".to_string(), val + nr);
                        } else {
                            my_map.insert("/".to_string(), nr);
                        }

                        for level in 0..current_path.len() {
                            let mut path: Vec<String> = Vec::new();
                            for l in 0..=level {
                                path.push(current_path[l].clone());
                            }
                            if my_map.contains_key(&get_path(&path)) {
                                let val = my_map.get(&get_path(&path)).unwrap();
                                my_map.insert(get_path(&path), val + nr);
                            } else {
                                my_map.insert(get_path(&path), nr);
                            }
                        }
                    }
                    _ => print!(""),
                }
            }
        }
    }
    if part == 1 {
        for (k, v) in my_map.iter() {
            if v <= &100_000 {
                total_size += v;
            }
            println!("key={}, value={}", k, v);
        }
    } else if part == 2 {
        total_size = 70_000_000;
        for (k, v) in my_map.iter() {
            if v >= &(30_000_000 - 70_000_000 + my_map.get("/").unwrap()) {
                println!(
                    "=== >>>> needed {} key={}, value={}",
                    30_000_000 - 70_000_000 + my_map.get("/").unwrap(),
                    k,
                    v
                );
                if v < &total_size {
                    total_size = *v;
                }
            } else {
                println!(
                    "unused {} key={}, value={}",
                    30_000_000 - 70_000_000 + my_map.get("/").unwrap(),
                    k,
                    v
                );
            }
        }
    }
    total_size
}

/// Part 1
#[aoc(day7, part1)]
fn part1(input: &str) -> i64 {
    calc_result(input, 1)
}

/// Part 2
#[aoc(day7, part2)]
fn part2(input: &str) -> i64 {
    calc_result(input, 2)
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "$ cd /
    $ ls
    dir a
    14848514 b.txt
    8504156 c.dat
    dir d
    $ cd a
    $ ls
    dir e
    29116 f
    2557 g
    62596 h.lst
    $ cd e
    $ ls
    584 i
    $ cd ..
    $ cd ..
    $ cd d
    $ ls
    4060174 j
    8033020 d.log
    5626152 d.ext
    7214296 k";

    #[test]
    fn part1_examples() {
        assert_eq!(95437, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(24933642, part2(&parse_input(EXAMPLE)));
    }
}
