//!

use std::{collections::HashMap, hash::Hash};

#[aoc_generator(day21)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

/// Part 1
#[aoc(day21, part1)]
fn part1(input: &str) -> i64 {
    let lines: Vec<_> = input.lines().collect();

    let mut values: HashMap<String, i64> = HashMap::new();

    let mut addition: HashMap<String, Vec<String>> = HashMap::new();
    let mut subtraction: HashMap<String, Vec<String>> = HashMap::new();
    let mut multiplication: HashMap<String, Vec<String>> = HashMap::new();
    let mut division: HashMap<String, Vec<String>> = HashMap::new();

    for line in lines {
        let split: Vec<&str> = line.trim().split(' ').collect();

        match split[1].parse::<i64>() {
            Ok(value) => {
                let mut key = split[0].to_string();
                key.pop();
                values.insert(key, value);
            }
            _ => {
                if split[2] == "+" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];

                    let mut key = split[0].to_string();
                    key.pop();

                    addition.insert(key, args);
                } else if split[2] == "-" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];

                    let mut key = split[0].to_string();
                    key.pop();

                    subtraction.insert(key, args);
                } else if split[2] == "*" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];

                    let mut key = split[0].to_string();
                    key.pop();

                    multiplication.insert(key, args);
                } else if split[2] == "/" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];

                    let mut key = split[0].to_string();
                    key.pop();

                    division.insert(key, args);
                }
            }
        }
    }

    while !values.contains_key("root") {
        //loop {
        //for i in 0..3 {

        for (k, v) in addition.iter() {
            if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                //println!("addition key={}, value={:?}", k, v);
                values.insert(k.to_string(), values[&v[0]] + values[&v[1]]);
            }
        }

        for (k, v) in subtraction.iter() {
            if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                //println!("subtraction key={}, value={:?}", k, v);
                values.insert(k.to_string(), values[&v[0]] - values[&v[1]]);
            }
        }

        for (k, v) in multiplication.iter() {
            if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                //println!("multiplication key={}, value={:?}", k, v);
                values.insert(k.to_string(), values[&v[0]] * values[&v[1]]);
            }
        }
        for (k, v) in division.iter() {
            if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                //println!("division key={}, value={:?}", k, v);
                values.insert(k.to_string(), values[&v[0]] / values[&v[1]]);
            }
        }

        for (k, v) in values.iter() {
            //println!("key={}, value={}", k, v);
        }

        if values.contains_key("root") {
            break;
        }
    }

    values["root"]
}

/// Part 2
#[aoc(day21, part2)]
fn part2(input: &str) -> i64 {
    let lines: Vec<_> = input.lines().collect();

    let mut values_start: HashMap<String, i64> = HashMap::new();

    let mut addition: HashMap<String, Vec<String>> = HashMap::new();
    let mut subtraction: HashMap<String, Vec<String>> = HashMap::new();
    let mut multiplication: HashMap<String, Vec<String>> = HashMap::new();
    let mut division: HashMap<String, Vec<String>> = HashMap::new();
    let mut equation: HashMap<String, Vec<String>> = HashMap::new();

    for line in lines {
        let split: Vec<&str> = line.trim().split(' ').collect();

        match split[1].parse::<i64>() {
            Ok(value) => {
                let mut key = split[0].to_string();
                key.pop();
                values_start.insert(key, value);
            }
            _ => {
                let mut key = split[0].to_string();
                key.pop();

                if key == "root" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];
                    equation.insert(key, args);
                } else if split[2] == "+" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];
                    addition.insert(key, args);
                } else if split[2] == "-" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];
                    subtraction.insert(key, args);
                } else if split[2] == "*" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];
                    multiplication.insert(key, args);
                } else if split[2] == "/" {
                    let args: Vec<String> = vec![split[1].to_string(), split[3].to_string()];
                    division.insert(key, args);
                }
            }
        }
    }

    for check in 3_882_224_466_000..=3_882_225_467_000 {
        println!("{}", check);
        let mut values = values_start.clone();

        values.insert("humn".to_string(), check);
        let mut found: bool = false;

        while !values.contains_key("root") {
            for (k, v) in equation.iter() {
                if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                    if values[&v[0]] == values[&v[1]] {
                        println!("they are equal {} = {}", values[&v[0]], values[&v[1]]);
                        values.insert("root".to_string(), values["humn"]);
                        found = true;
                    } else {
                        println!("they are NOT equal diff {}", values[&v[0]] - values[&v[1]]);
                        values.insert("root".to_string(), values["humn"]);
                    }
                }
            }
            for (k, v) in addition.iter() {
                if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                    values.insert(k.to_string(), values[&v[0]] + values[&v[1]]);
                }
            }

            for (k, v) in subtraction.iter() {
                if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                    values.insert(k.to_string(), values[&v[0]] - values[&v[1]]);
                }
            }

            for (k, v) in multiplication.iter() {
                if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                    values.insert(k.to_string(), values[&v[0]] * values[&v[1]]);
                }
            }
            for (k, v) in division.iter() {
                if values.contains_key(&v[0]) && values.contains_key(&v[1]) {
                    values.insert(k.to_string(), values[&v[0]] / values[&v[1]]);
                }
            }
        }

        if found {
            return values["root"];
        }
    }
    1
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "root: pppw + sjmn
    dbpl: 5
    cczh: sllz + lgvd
    zczc: 2
    ptdq: humn - dvpt
    dvpt: 3
    lfqf: 4
    humn: 5
    ljgn: 2
    sjmn: drzm * dbpl
    sllz: 4
    pppw: cczh / lfqf
    lgvd: ljgn * ptdq
    drzm: hmdt - zczc
    hmdt: 32";

    #[test]
    fn part1_examples() {
        assert_eq!(152, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(301, part2(&parse_input(EXAMPLE)));
    }
}
