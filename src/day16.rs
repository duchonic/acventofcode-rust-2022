//!
use std::collections::HashMap;

#[aoc_generator(day16)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

struct Valve {
    rate: i32,
    tunnels: Vec<String>,
    open: bool,
}

impl Valve {
    fn new(rate: i32) -> Valve {
        let tunnels: Vec<String> = Vec::new();
        let open = false;
        let valve = Valve {
            rate,
            tunnels,
            open,
        };
        valve
    }
    fn add_tunnel(&mut self, name: String) {
        self.tunnels.push(name);
    }
    fn show(&mut self) {
        println!("rate:{} open:{} ", self.rate, self.open);
        for tunnel in self.tunnels.iter() {
            println!("tunnel to : {}", tunnel);
        }
    }
    fn open(&mut self) {
        self.open = true;
    }
}

/// Part 1
#[aoc(day16, part1)]
fn part1(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();

    let mut valves: HashMap<String, Valve> = HashMap::new();

    for line in lines {
        let cmd: Vec<&str> = line.trim().split(' ').collect();

        let name = cmd[1].to_string();
        let rate = cmd[4].replace(";", "");
        let rate: Vec<&str> = rate.split('=').collect();

        let mut valve: Valve;
        match rate[1].parse::<i32>() {
            Ok(rate) => {
                valve = Valve::new(rate);
            }
            _ => {
                valve = Valve::new(1);
            }
        }
        for tunnel in 9..cmd.len() {
            let name = cmd[tunnel].replace(",", "");
            valve.add_tunnel(name);
        }
        valves.insert(name, valve);
    }

    let mut actual_valve: &String = &"AA".to_string();
    let mut next_valve: String = "AA".to_string();

    let mut total_pressure: i32 = 0;
    for minutes in 0..30 {
        let mut actual = valves.get(&next_valve).unwrap();

        if actual.open {
            println!("valve is open");
        } else {
            //actual.open();
        }
        println!("{} min", minutes);
        next_valve = actual.tunnels[0].clone();
        println!("{} is next", next_valve);
    }

    total_pressure
}

/// Part 2
#[aoc(day16, part2)]
fn part2(input: &str) -> i32 {
    let result: Vec<_> = input.lines().collect();
    11
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
    Valve BB has flow rate=13; tunnels lead to valves CC, AA
    Valve CC has flow rate=2; tunnels lead to valves DD, BB
    Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
    Valve EE has flow rate=3; tunnels lead to valves FF, DD
    Valve FF has flow rate=0; tunnels lead to valves EE, GG
    Valve GG has flow rate=0; tunnels lead to valves FF, HH
    Valve HH has flow rate=22; tunnel leads to valve GG
    Valve II has flow rate=0; tunnels lead to valves AA, JJ
    Valve JJ has flow rate=21; tunnel leads to valve II";

    #[test]
    fn part1_examples() {
        //assert_eq!(1651, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        //assert_eq!(45000, part2(&parse_input(EXAMPLE)));
    }
}
