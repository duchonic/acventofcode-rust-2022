//!
#[aoc_generator(day4)]
fn parse_input(input: &str) -> String {
    input.to_string()
}

/// Part 1
#[aoc(day4, part1)]
fn part1(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();
    let mut total = 0;

    for line in lines {
        let sections = line.trim().split(',');
        let mut values: Vec<i32> = Vec::new();
        for section in sections {
            let start_stop = section.split('-');
            for s in start_stop {
                values.push(s.parse::<i32>().unwrap());
            }
        }
        if values[0] >= values[2] && values[1] <= values[3]
            || values[0] <= values[2] && values[1] >= values[3]
        {
            total += 1;
        }
        values.clear();
    }

    total
}

/// Part 2
#[aoc(day4, part2)]
fn part2(input: &str) -> i32 {
    let lines: Vec<_> = input.lines().collect();
    let mut total = 0;

    for line in lines {
        let sections = line.trim().split(',');
        let mut values: Vec<i32> = Vec::new();
        for section in sections {
            let start_stop = section.split('-');
            for s in start_stop {
                values.push(s.parse::<i32>().unwrap());
            }
        }
        if values[1] >= values[2] && values[3] >= values[0] {
            total += 1;
        }
        values.clear();
    }
    total
}

#[cfg(test)]
mod tests {
    use super::*;

    const EXAMPLE: &str = "2-4,6-8
    2-3,4-5
    5-7,7-9
    2-8,3-7
    6-6,4-6
    2-6,4-8
    4-5,1-2";

    #[test]
    fn part1_examples() {
        assert_eq!(2, part1(&parse_input(EXAMPLE)));
    }

    #[test]
    fn part2_examples() {
        assert_eq!(4, part2(&parse_input(EXAMPLE)));
    }
}
