# Advent of Code 2021 Solutions in Rust

Solutions written in the [Rust Programming Language](https://www.rust-lang.org/) for Advent of Code:

> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

- ❔ [about Advent of Code](https://adventofcode.com/about)
- 📆 [list of problems](https://adventofcode.com/2022)

Should be compatible with Windows, Linux and macOS.

## 🛠️ Requirements

- [rust / cargo](https://rustup.rs/)
- [cargo-aoc](https://github.com/gobanos/cargo-aoc)

Install cargo-aoc with:
```bash
cargo install cargo-aoc 
```

## 👷 Installation

Clone this repository and change into the directory.

## 🚀 Usage  

Execute specific day, in this example `day 1`:

```bash
cargo aoc -d 1
```

## 🧪 Run tests

```bash
cargo test
```
